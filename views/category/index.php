<script>
    function confirmDelete() {
      if (confirm('Bạn có chắn chắn xóa?')) {
        return true;
      }
      return false;
    }
</script>

<div class="container">
  <h2>Tất cả category</h2>
  <div class="alert alert-primary" role="alert">
      <?php
      if (isset($_SESSION['update_success'])){
        echo $_SESSION['update_success'];
        unset($_SESSION['update_success']);
      }
      ?>
  </div>        
  <table class="table table-striped">
  <a href="?controller=CategoryController&action=ShowFormCreate" class="btn btn-primary">Add Category</a>
    <thead>
      <tr>
        <th>Name</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($categories as $cate) {?>
      <tr>
        <td><?= $cate['name']?></td>
        <td>
            <?php
                if ($cate['status'] == 1) {
                    echo 'Active';
                } else {
                    echo 'Inactive';
                }
            ?>
        </td>
        <td>
            <a href="?controller=CategoryController&action=ShowFormEdit&id=<?= $cate['id']?>">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a onclick="return confirmDelete()" href="?controller=CategoryController&action=DeleteCategory&id=<?= $cate['id']?>">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
        </td>
      </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
