
<form action="?controller=CategoryController&action=Create" method="POST">

    <div class="form-group">
        <legend>Add Category</legend>
    </div>

    <div class="form-group">
      <p style="color: red">
        <?php
          if(isset($_SESSION['empty'])) {
            echo $_SESSION['empty'];
            unset($_SESSION['empty']);
          }
        ?>
      </p>
    </div>

  <div class="form-group">
    <label for="email">Name:</label>
    <input type="text" name="name" class="form-control">
  </div>
  <div class="checkbox">
    <label><input type="checkbox" value="1" name="status"> Active</label>
  </div>
  <button type="submit" name="add" class="btn btn-default">ADD</button>
</form>