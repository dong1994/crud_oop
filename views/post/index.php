<script>
    function confirmDelete() {
      if (confirm('Bạn có chắn chắn xóa?')) {
        return true;
      }
      return false;
    }
</script>

<div class="container">
  <h2>Tất cả category</h2>
  <div class="alert alert-primary" role="alert">
      <?php
      if (isset($_SESSION['update_success'])){
        echo $_SESSION['update_success'];
        unset($_SESSION['update_success']);
      }
      ?>
  </div>        
  <table class="table table-striped">
  <a href="?controller=PostController&action=ShowFormCreate" class="btn btn-primary">Add Post</a>
    <thead>
      <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($posts as $post) {?>
      <tr>
        <td><?= $post['title']?></td>
        <td><?= $post['description']?></td>
        <td>
            <?php
                if ($post['status'] == 1) {
                    echo 'Active';
                } else {
                    echo 'Inactive';
                }
            ?>
        </td>
        <td>
            <a href="?controller=PostController&action=ShowFormEdit&id=<?= $post['id']?>">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a onclick="return confirmDelete()" href="?controller=PostController&action=DeleteCategory&id=<?= $post['id']?>">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
        </td>
      </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
