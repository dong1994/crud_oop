<?php

$idCategory = isset($_GET['id']) ? $_GET['id'] : "";

$category = $category->getCategoryByID($idCategory);

?>

<form action="?controller=CategoryController&action=Update" method="POST">

    <input type="hidden" name="idCategory" value="<?= $category['id']?>">

    <div class="form-group">
        <legend>Edit Category <?= $category['name']?></legend>
    </div>

    <div class="form-group">
      <p style="color: red">
        <?php
          if(isset($_SESSION['empty'])) {
            echo $_SESSION['empty'];
            unset($_SESSION['empty']);
          }
        ?>
      </p>
    </div>

  <div class="form-group">
    <label for="email">Name:</label>
    <input type="text" name="name" value="<?= $category['name']?>" class="form-control">
  </div>
  <div class="checkbox">
    <label><input 
            type="checkbox" 
            value="1"
            <?php
            if ($category['status'] == 1) {
                echo 'checked';
            }
            ?>
            name="status"
    > Active</label>
  </div>
  <button type="submit" name="update" class="btn btn-default">UPDATE</button>
</form>