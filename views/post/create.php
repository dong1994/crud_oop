


<form action="?controller=CategoryController&action=Create" method="POST">

    <div class="form-group">
        <legend>Add Post</legend>
    </div>

    <div class="form-group">
      <p style="color: red">
        <?php
          if(isset($_SESSION['empty'])) {
            echo $_SESSION['empty'];
            unset($_SESSION['empty']);
          }
        ?>
      </p>
    </div>

  <div class="form-group">
    <label for="sel1">Category:</label>
    <select class="form-control" name="category_id">
      <option>Please choose category</option>
      <?php foreach($category as $cat) {?>
      <option value="<?= $cat['id']?>"> <?= $cat['name']?></option>
      <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="">Title:</label>
    <input type="text" name="title" class="form-control">
  </div>
  <div class="form-group">
    <label for="">Description:</label>
    <textarea name="description" type="text" name="description" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <label for="">Content:</label>
    <textarea name="content" type="text" name="content" class="form-control"></textarea>
  </div>
  <div class="checkbox">
    <label><input type="checkbox" value="1" name="status"> Active</label>
  </div>
  <button type="submit" name="add" class="btn btn-default">ADD</button>
</form>

<script>
    CKEDITOR.replace('description');
    CKEDITOR.replace('content');
</script>