
<?php

if (isset($_POST['login'])) {
    $result = $user->Login();
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
</head>
<body>
    
    <form method="POST" role="form">
        <legend>Login To System</legend>

        <p style="color: red">
            <?php
            if (isset($result) && count($result) > 0) {
                echo $result['error'];
            }
            ?>
        </p>
        <div class="form-group">
            <label for="">Email</label>
            <input type="email" class="form-control" id="" name="email" placeholder="Input Email">
        </div>
    
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" id="" name="password" placeholder="Input Password">
        </div>
        <button type="submit" class="btn btn-primary" name="login">Login</button>
    </form>
    
</body>
</html>