<?php
require 'models/UserModel.php';

class UserController {

    private $userModel;
    public function  __construct()
    {
        $this->userModel = new UserModel();  
    }

    public function Login() {
        $errors = array();
        $email = $_POST['email'];
        $password = $_POST['password'];
        if(empty($email) || empty($password)) {
            $errors['error'] = "Email or password cannot be empty";
            return $errors;
        } else {
            $user = $this->userModel->LoginUser($email, $password);
            if ($user !== false) {
                $_SESSION['user_info'] = $user;
                header("Location: ?controller=HomeController&action=Index");
            } else {
                $errors['error'] = "Email or password incorrect";
                // header("Location: ?controller=UserController&action=ShowFormLogin");
                return $errors;
            }
        }
        
    }

    public function Logout() {
        unset($_SESSION['user_info']);
        header("Location: ?controller=UserController&action=ShowFormLogin");
    }
}

?>