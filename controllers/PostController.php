<?php

require_once('models/PostModel.php');
require_once('models/CategoryModel.php');

class PostController {

    private $postModel;

    public function __construct()
    {
        $this->postModel = new PostModel();
    }

    public function index() {
        $posts = $this->postModel->getAllPosts();
        return $posts;
    }

    public function getAllCategories() {
        $categories = new CategoryModel();
        return $categories->getAllCategoriesStatusActive();
    }

    // public function addCategory() {
    //     $name = isset($_POST['name']) ? $_POST['name']: '';
    //     $status = isset($_POST['status']) ? $_POST['status']: '';
    //     if(!empty($name)) {
    //         $result = $this->categoryModel->createCategory($name, $status);
    //         if($result > 0) {
    //             header("Location: ?controller=CategoryController&action=Index");
    //         }
    //     } else {
    //         $_SESSION['empty'] = 'Name cannot be emptty';
    //         header("Location: ?controller=CategoryController&action=ShowFormCreate");
    //     }
    // }

    // public function getCategoryByID($idCategory) {
    //     $sql = "SELECT * FROM category WHERE id = $idCategory";
    //     $result = $this->categoryModel->getRecord($sql);
    //     return $result;
    // }

    // public function updateCategory() {
    //     if (isset($_POST['update'])) {
    //         $name = $_POST['name'];
    //         $status = $_POST['status'];
    //         $idCategory = $_POST['idCategory'];
    //         $result = $this->categoryModel->updateCategory($name, $status, $idCategory);
    //         if ($result > 0) {
    //             $_SESSION['update_success'] = "Update Success";
    //             header("Location: ?controller=CategoryController&action=Index");
    //         }
    //     }
    // }
    
    // public function DeleteCategory() {
    //     $idCategory = isset($_GET['id']) ? $_GET['id'] : '';
    //     if (!empty($idCategory)) {
    //         $result = $this->categoryModel->deleteCategory($idCategory);
    //         if ($result > 0) {
    //             header("Location: ?controller=CategoryController&action=Index");
    //         }
    //     }
    // }

}

?>