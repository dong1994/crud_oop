<?php

class Connection {

    public function __construct()
    {
        $conn = null;
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $databasename = 'blogtintuc';
        $charset = 'utf8';

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];
        $dsn = "mysql:host=$host;dbname=$databasename;charset=$charset";
        try {
            global $conn;
            if (!$conn) {
                $conn = new PDO($dsn, $username, $password, $options);
            }
            // echo 'Connect success!';
        
        } catch (PDOException $e) {
            echo 'Connection faild: '.$e->getMessage();
        }
    }

    function getAllRecords($sql) {
        global $conn;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    function getRecord($sql) {
        global $conn;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    function executeSQL($sql) {
        global $conn;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount();
    }

    function disconnect() {
        global $conn;
        if ($conn) {
            $conn = null;
        }
    }

}

?>