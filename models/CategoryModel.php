<?php

class CategoryModel extends Connection {

    private $connection;
    
    public function __construct()
    {
        $this->connection = new Connection();   
    }

    function getAllCategories() {
        $sql = "SELECT * FROM category";
        $result = $this->connection->getAllRecords($sql);
        return $result;
    }

    function getAllCategoriesStatusActive() {
        $sql = "SELECT * FROM category WHERE status = 1";
        $result = $this->connection->getAllRecords($sql);
        return $result;
    }

    function getCategoryByID($id) {
        $sql = "SELECT * FROM category WHERE id = $id";
        $result = $this->connection->getRecord($sql);
        return $result;
    }

    function createCategory($name, $status) {
        $sql = "INSERT INTO category(name, status) VALUES('$name', '$status')";
        $result = $this->connection->executeSQL($sql);
        return $result;
    }

    public function updateCategory($name, $status, $idCategory) {
        $sql = "UPDATE category SET name='$name', status = '$status' WHERE id = $idCategory";
        $result = $this->connection->executeSQL($sql);
        return $result;
    }

    public function deleteCategory( $idCategory) {
        $sql = "DELETE FROM category WHERE id = $idCategory";
        $result = $this->connection->executeSQL($sql);
        return $result;
    }

    
}
?>