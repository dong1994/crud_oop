<?php

session_start();

require_once('config/Connection.php');
require_once('config/Path.php');

if (isset($_GET['controller'], $_GET['action'])) {
    if(!isset($_SESSION['user_info'])) {
        $controller = 'UserController';
        $action = "ShowFormLogin";
    } else {
        $controller = $_GET['controller'];
        $action = $_GET['action'];
    }
} else {
    if (!isset($_SESSION['user_info'])) {
        $controller = 'UserController';
        $action = "ShowFormLogin";
    } else {
        $controller = 'HomeController';
        $action = 'Index';
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    
</head>
<body>
    <div class="container">

    <?php if (isset($_SESSION['user_info'])) { ?>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>QUẢN LÝ HỆ THỐNG</h1>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <a href="?controller=CategoryController&action=Index">Quản Lý Category</a>
            </div>
            <div class="col-md-3">
            <a href="?controller=PostController&action=Index">Quản Lý Posts</a>
            </div>
            <div class="col-md-3">
                <a href="?controller=UserController&action=Index">Quản Lý Users</a>
            </div>
            <div class="col-md-3">
                <a href="?controller=UserController&action=Logout">Logout</a>
            </div>
        </div>
    <?php }?>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top: 20px">
                <?php
                switch ($controller) {
                    case 'CategoryController':
                        require_once('controllers/CategoryController.php');
                        $category = new CategoryController();
                        if ($action == 'ShowFormCreate') {
                            require_once('views/category/create.php');
                        } else if($action == "Create") {
                            $category->addCategory();
                        } else if($action == "ShowFormEdit") {
                            require_once('views/category/edit.php');
                        } else if ($action == "Update") {
                            $category->updateCategory();
                        } else if ($action == 'DeleteCategory'){ 
                            $category->DeleteCategory();
                        } else {
                            $categories = $category->index();
                            require_once('views/category/index.php');
                        }
                        
                        break;
                    case 'PostController':
                        require_once('controllers/PostController.php');
                        $post = new PostController();
                        if ($action == 'ShowFormCreate') {
                            $category = $post->getAllCategories();
                            require_once('views/post/create.php');
                        } else if($action == "Create") {
                            $post->addPost();
                        } else if($action == "ShowFormEdit") {
                            require_once('views/post/edit.php');
                        } else if ($action == "Update") {
                            $post->updatePost();
                        } else if ($action == 'DeletePost'){ 
                            $post->DeletePost();
                        } else {
                            $posts = $post->index();
                            require_once('views/post/index.php');
                        }
                        
                        break;
                    case 'UserController':
                        require_once('controllers/UserController.php');
                        $user = new UserController();
                        if ($action == "ShowFormLogin") {
                            require_once('views/user/login.php');
                        } else if ($action == "Logout") {
                            $user->Logout();
                        } else {
                            require_once('views/user/index.php');
                        }
                        
                        break;
                    default:
                        require_once('controllers/HomeController.php');
                        $home = new HomeController();
                        require_once('views/home/index.php');
                        break;
                }
                ?>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</body>
</html>